package com.denis.fomin.lab5_2

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

data class PaymentState(
    val hoursInputText: String = "2",
    val discount: Float = 10F,
    val price: Float = 2000F,
)


class MainViewModel: ViewModel() {
    private val _state = MutableStateFlow(PaymentState())
    val state: StateFlow<PaymentState> = _state.asStateFlow()

    fun setHours(hours: String) {
        _state.update {currentState ->
            currentState.copy(
                hoursInputText = hours,
            )
        }
    }

    fun setDiscount(discount: Float) {
        _state.update {currentState ->
            currentState.copy(
                discount = discount,
            )
        }
    }
}