package com.denis.fomin.lab5_2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.text.isDigitsOnly
import com.denis.fomin.lab5_2.ui.theme.Lab5_2Theme
import kotlin.math.roundToInt
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Lab5_2Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Lab5_2Theme {
                        ApplicationNavHost()
                    }
                }
            }
        }
    }
}

@Composable
fun ApplicationNavHost(
    navController: NavHostController = rememberNavController(),
    startDestination: String = "hoursPayment",
    viewModel: MainViewModel = viewModel(),
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
    ) {
        composable("hoursPayment") {
            HoursPaymentScreen(
                viewModel = viewModel,
                onOkClick = {
                    navController.navigate("finalPayment")
                }
            )
        }
        composable("finalPayment") {
            FinalPaymentScreen(
                viewModel = viewModel,
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HoursPaymentScreen(
    onOkClick: () -> Unit,
    viewModel: MainViewModel = viewModel()
) {
    val uiState by viewModel.state.collectAsState()

    val price by uiState::price
    val text by uiState::hoursInputText
    val discount by uiState::discount

    val fullWidthModifier = Modifier.fillMaxWidth()

    Column(
        modifier = Modifier.padding(10.dp)
    ) {
        Text(
            text = stringResource(R.string.hours_amount),
        )
        TextField(
            value = text,
            onValueChange = {
                if (it.isEmpty() || it.isDigitsOnly()) {
                    viewModel.setHours(it)
                }
            },
            modifier = Modifier.padding(
                top = 10.dp,
                bottom = 10.dp,
            ),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        Text(
            text = "$price ${stringResource(R.string.payment_rate)}"
        )
        Text(
            text = "${stringResource(R.string.discount)} (${discount.roundToInt()}%)",
            modifier = Modifier.padding(top = 40.dp)
        )
        Slider(
            value = discount,
            onValueChange = { viewModel.setDiscount(it) },
            steps = 9,
            valueRange = 0F..100F,
        )
        Row(
            modifier = fullWidthModifier,
        ) {
            Button(
                onClick = onOkClick,
                modifier = fullWidthModifier.padding(
                    top = 30.dp
                ),
                enabled = text.isNotEmpty(),
            ) {
                Text(
                    text = "OK",
                )
            }
        }
    }
}

@Composable
fun FinalPaymentScreen(viewModel: MainViewModel = viewModel()) {
    val uiState by viewModel.state.collectAsState()

    val text by uiState::hoursInputText
    val discount by uiState::discount

    val hours = text.toFloat()

    val finalPrice = hours - hours * (discount / 100)

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize(),
    ) {
        Text(
            text = "${stringResource(R.string.need_to_pay)} = $finalPrice ${stringResource(R.string.currency)}"
        )
    }
}

@Preview(
    showBackground = true,
    showSystemUi = true,
)
@Composable
fun HoursPaymentScreenPreview() {
    Lab5_2Theme {
        HoursPaymentScreen(
            onOkClick = {}
        )
    }
}

@Preview(
    showBackground = true,
    showSystemUi = true,
)
@Composable
fun FinalPaymentScreenPreview() {
    Lab5_2Theme {
        FinalPaymentScreen()
    }
}
